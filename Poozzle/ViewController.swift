//
//  ViewController.swift
//  Poozzle
//
//  Created by Neal Watkins on 2020/1/16.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa
import SpriteKit
import GameplayKit

class ViewController: NSViewController {
    
    @IBOutlet var puzzleView: SKView!
    
    
    var puzzleScene: PuzzleScene? {
        return puzzleView.scene as? PuzzleScene
    }
    
    @IBAction func solve(_ sender: Any) {
        puzzleScene?.solve()
    }
    
    @IBAction func shuffle(_ sender: Any) {
        build()
        puzzleScene?.shuffle()
    }
    
    @IBAction func browseImage(_ sender: Any) {
        let panel = NSOpenPanel()
        panel.allowsMultipleSelection = false
        panel.canChooseDirectories = false
        panel.allowedFileTypes = ["jpg","png",".gif",".tif",".tiff",".jpeg",".pdf"]
        let ok = panel.runModal()
        guard ok == .OK else { return }
        guard let url = panel.url else { return }
        UserDefaults.standard.set(url, forKey: "Image")
        paint()
    }
    
    var image: NSImage? {
        guard let url = UserDefaults.standard.url(forKey: "Image") else { return nil}
        return NSImage(contentsOf: url)
        
    }
    
    func build() {
        guard let scene = SKScene(fileNamed: "PuzzleScene") else { return }
        scene.scaleMode = .aspectFit
        scene.anchorPoint = .zero
        puzzleView.presentScene(scene)
        puzzleScene?.build()
        paint()
    }
    
    func paint() {
        let image = self.image ?? Default.image
        puzzleScene?.paint(with: image)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerDefaults()
        build()
    }
    
    
    
    struct Default {
        static var image: NSImage {
            return NSImage(named: "Rainbow")!
        }
    }
        
    func registerDefaults() {
        let plist = Bundle.main.path(forResource: "Defaults", ofType: "plist")
        if let dic = NSDictionary(contentsOfFile: plist!) as? [String: Any] {
            UserDefaults.standard.register(defaults: dic)
        }
    }
    
}

