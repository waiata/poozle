//
//  PuzzleScene.swift
//  Poozzle
//
//  Created by Neal Watkins on 2020/1/16.
//  Copyright © 2020 Waiata. All rights reserved.
//

import SpriteKit
import GameplayKit

class PuzzleScene: SKScene {
    
    var surround = SKShapeNode()
    var dot = SKShapeNode()
    var pieces = [Piece]()
    var places = [Place]()
    var moves = 0 {
        didSet {
            renderMoves()
        }
    }
    
    override func didMove(to view: SKView) {
    }
    
    override func didChangeSize(_ oldSize: CGSize) {
        resize()
    }
    
    var tileside: CGFloat? {
        guard let bounds = view?.bounds else { return nil }
        let w = bounds.width / CGFloat( Default.columns )
        let h = bounds.height / CGFloat( Default.rows + 1 )
        return min(w,h)
    }
    
    var tilesize: CGSize? {
        guard let side = tileside else { return nil }
        return CGSize(width: side, height: side)
    }
    
    func renderMoves() {
        guard let toolbar = view?.window?.toolbar else { return }
        let items = toolbar.items.filter{ $0 is MovesToolbarItem } as! [MovesToolbarItem]
        _ = items.map{ $0.moves = moves }
    }
    
    func build() {
        buildSurround()
        buildPlaces()
        buildPieces()
    }
    
    func buildSurround() {
        guard let side = tileside else { return }
        guard let bounds = view?.bounds else { return }
        self.size = bounds.size
        let w = CGFloat(Default.columns) * side
        let h = CGFloat(Default.rows + 1) * side
        let o = CGPoint(x: (bounds.width - w)/2, y: (bounds.height - h)/2)
        anchorPoint = .zero
        surround.position = o
        let path = CGMutablePath()
        path.move(to: CGPoint.zero)
        path.addLine(to: CGPoint(x: 0, y: h))
        path.addLine(to: CGPoint(x: w, y: h))
        path.addLine(to: CGPoint(x: w, y: side))
        path.addLine(to: CGPoint(x: side, y: side))
        path.addLine(to: CGPoint(x: side, y: 0))
        path.closeSubpath()
        surround.path = path
        surround.fillColor = NSColor.darkGray
        surround.strokeColor = NSColor.black
        surround.lineWidth = 2.0
        addChild(surround)
    }
    
    func showDot(at: CGPoint = .zero) {
        dot = SKShapeNode(circleOfRadius: 2.0)
        dot.fillColor = .red
        dot.lineWidth = 2.0
        dot.strokeColor = .white
        addChild(dot)
    }
    
    func buildPlaces() {
        places = []
        let columns = Default.columns
        let rows = Default.rows
        guard columns > 0, rows > 0 else { return }
        
        for c in 1...columns {
            for r in 1...rows {
                let place = Place(column: c, row: r)
                places.append(place)
            }
        }
        places.append(Place(column: 1, row: 0)) /// spare slot
    }

    func buildPieces() {
        pieces = [Piece]()
        surround.removeAllChildren()
        
        for place in placesToFill {
            let piece = Piece(at: place)
            surround.addChild(piece.sprite)
            pieces.append(piece)
        }
        resize()
    }
    
    var placesToFill: [Place] {
        var fills = self.places
        _ = fills.popLast()
        return fills
    }
    
    
    func solve() {
        for piece in pieces {
            piece.goHome()
        }
    }
    
    func shuffle() {
        solve()
        for _ in (1...Default.shuffleSteps) {
            randomMove()
        }
        moves = 0
    }
    
    func randomMove() {
        for gap in empty {
            let moveable = pieces.filter{ $0.next(to: gap) }
            guard let piece = moveable.randomElement() else { continue }
            piece.position(at: gap)
        }
    }
    
    func randomize() {
        var random = placesToFill
        random.removeFirst()
        random.shuffle()
        for i in 1..<pieces.count {
            let place = random.removeFirst()
            pieces[i].move(to: place)
        }
    }
    
    func paint(with image: NSImage) {
        let tileWidth = image.size.width / CGFloat(Default.columns)
        let tileHeight = image.size.height / CGFloat(Default.rows)
        let side = min(tileWidth, tileHeight)
        let paddingX = (image.size.width - (side * CGFloat(Default.columns))) / 2
        let paddingY = (image.size.height - (side * CGFloat(Default.rows))) / 2
        let size = CGSize(width: side, height: side)
        guard side != 0 else { return }
        for piece in pieces {
            let origin = CGPoint(x: piece.home.x * side + paddingX, y: piece.home.y * side + paddingY)
            let rect = CGRect(origin: origin, size: size)
            let crop = NSImage(size: size)
            crop.lockFocus()
            image.draw(at: .zero, from: rect, operation: .copy, fraction: 1.0)
            crop.unlockFocus()
            piece.paint(image: crop)
        }
    }
    
    
    func resize() {
        guard let size = tilesize else { return }
        for piece in pieces {
            piece.size = size
        }
    }
    
    override func mouseDown(with event: NSEvent) {
        guard let piece = self.piece(under: event.location(in: surround)) else { return }
        slide(piece)
    }
    
    override func keyDown(with event: NSEvent) {
        if let sk = event.specialKey {
            switch sk {
                case .leftArrow : slide(-1,0)
                case .upArrow : slide(0,1)
                case .rightArrow : slide(1,0)
                case .downArrow : slide(0,-1)
            default: break
            }
        }
    }
    
    func piece(under location: CGPoint) -> Piece? {
        for piece in pieces {
            if piece.isUnder(location) { return piece }
        }
        return nil
    }
    
    func piece(at: Place) -> Piece? {
        for piece in pieces {
            if piece.place == at { return piece }
        }
        return nil
    }
    
    func slide(_ piece: Piece) {
        for gap in empty {
            if piece.next(to: gap) {
                piece.move(to: gap)
                check()
            }
        }
    }
    
    func slide(_ x: Int, _ y: Int) {
        for gap in empty {
            let place = Place(column: gap.column - x, row: gap.row - y)
            if let piece = piece(at: place) {
                piece.move(to: gap)
                check()
            }
        }
    }
    
    var empty: [Place] {
        return places.filter { !occupied.contains($0) }
    }
    
    
    var occupied: [Place] {
        return pieces.map { $0.place }
    }
    
    func check() {
        moves += 1
        let wrong = pieces.filter{ !$0.isHome }
        if wrong.count == 0 { print("Done in \(moves) moves") }
    }
    
}

extension PuzzleScene {
    struct Default {
        static let defaults = UserDefaults.standard
        
        static var columns: Int {
            return defaults.integer(forKey: "Columns")
        }
        static var rows: Int {
            return defaults.integer(forKey: "Rows")
        }
        static var shuffleSteps: Int {
            return defaults.integer(forKey: "ShuffleSteps")
        }
    }
}
