//
//  MovesToolbarItem.swift
//  Poozzle
//
//  Created by Neal Watkins on 2020/3/7.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa

class MovesToolbarItem: NSToolbarItem {

    var textField: NSTextField? {
        return view as? NSTextField
    }
    
    var moves: Int = 0 {
        didSet {
            textField?.integerValue = moves
        }
    }
}
