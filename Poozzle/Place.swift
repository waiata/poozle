//
//  Place.swift
//  Poozzle
//
//  Created by Neal Watkins on 2020/1/16.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Foundation

struct Place {
    
    var column: Int
    var row: Int
    
    var x: CGFloat {
        return CGFloat(column - 1)
    }
    var y: CGFloat {
        return CGFloat(row - 1)
    }
    
    func position(size: CGSize) -> CGPoint {
        return CGPoint(x: (x + 0.5) * size.width, y: (y + 1.5) * size.height)
    }
    
    var isOpen: Bool {
        return row > 0 && column != 0
    }
}

extension Place: Equatable {
    
    static func ==(lhs: Place, rhs: Place) -> Bool {
        return lhs.column == rhs.column && lhs.row == rhs.row
    }
}
