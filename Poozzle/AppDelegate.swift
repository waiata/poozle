//
//  AppDelegate.swift
//  Poozzle
//
//  Created by Neal Watkins on 2020/1/16.
//  Copyright © 2020 Waiata. All rights reserved.
//


import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    
    func applicationWillFinishLaunching(_ aNotification: Notification) {
        
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
   
}
