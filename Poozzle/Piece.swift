//
//  Piece.swift
//  Poozzle
//
//  Created by Neal Watkins on 2020/1/16.
//  Copyright © 2020 Waiata. All rights reserved.
//

import Cocoa
import SpriteKit

class Piece {
    
    var home: Place
    var place: Place
    
    var sprite = SKShapeNode() {
        didSet {
            let parent = oldValue.parent
            oldValue.removeFromParent()
            parent?.addChild(sprite)
        }
    }
    
    var size: CGSize = CGSize.zero {
        didSet {
            render()
        }
    }
    
    
    init(at: Place) {
        self.home = at
        self.place = at
    }
    
    func isUnder(_ location: CGPoint) -> Bool {
        return sprite.contains(location)
    }
    
    var isHome: Bool {
        return home == place
    }
    
    func goHome() {
        position(at: home)
    }
    
    func paint(image: NSImage) {
        let texture = SKTexture(image: image)
        sprite.fillTexture = texture
    }
    
    func render() {
        sprite = SKShapeNode(rectOf: size, cornerRadius: Default.rounding)
        colour()
        position()
    }
    
    func colour() {
        sprite.lineWidth = 2.0
        sprite.strokeColor = NSColor.black
        sprite.fillColor = NSColor.white
    }
    
    func position(at: Place? = nil) {
        let place = at ?? self.place
        self.place = place
        let xy = place.position(size: size)
        sprite.position = xy
    }
    
    func move(to: Place? = nil) {
        place = to ?? place
        let xy = place.position(size: size)
        let move = SKAction.move(to: xy, duration: Default.speed)
        sprite.run(move)
    }
    
    func next(to: Place) -> Bool {
        let steps = abs(place.x - to.x) + abs(place.y - to.y)
        return steps < 2
    }
    
    struct Default {
        static let defaults = UserDefaults.standard
        static var rounding: CGFloat {
            return CGFloat(defaults.float(forKey: "Rounding"))
        }
        static var speed: TimeInterval {
            return defaults.double(forKey: "Speed")
        }
    }
    
}

extension Piece : CustomStringConvertible {
    var description: String {
        return "Piece \(home.x),\(home.y) @ \(place.x),\(place.y)"
    }
    
    
}
